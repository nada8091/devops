import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
const namespaceName = `devops`;

const provider = new k8s.Provider("provider", {kubeconfig: "kubeconfig.yaml" , cluster: "clusterdea48990", context: "clusterdea48990"});
const jenkins = new k8s.helm.v3.Chart("jenkins-helm", {
	    namespace: namespaceName,
	    path: "../jenkins-k8s"
}, { provider: provider });


export let serviceIP = jenkins.getResourceProperty("v1/Service", "devops/jenkins-helm-jenkins-k8s", "status").apply(status => status.loadBalancer.ingress[0].ip);
        

